#!/bin/sh

#  NodeConfiguration.sh
#  
#
#  Created by David Kittell on 6/1/17.
#

clear

OS=$(cat /etc/redhat-release)
echo $OS

banner=$(echo "\n$OS\n")
banner=${banner}$(echo "$(companyname)\n")
banner=${banner}$(echo "Hostname: $(hostname)\n")
banner=${banner}$(echo "Network Information\n")

NetworkPorts=$(ip link show | grep '^[a-z0-9]' | awk -F : '{print $2}')
for val in $(echo $NetworkPorts); do   # Get for all available hardware ports their status
#echo "Current Interface: $val"
netActive=$(ifconfig $val | grep "inet.*broadcast" -B1 | grep "flags" | cut -d ":" -f1)
echo $netActive

if [ "$netActive" == "$val" ]
then
netIP=$(/sbin/ip -o -4 addr list $val | awk '{print $4}' | cut -d/ -f1)
netMask=$(ipcalc -m $netIP | cut -d '=' -f2)
netCIDR=$(ipcalc -p $netIP $netMask | cut -d '=' -f2)
netWork=$(ipcalc -n $netIP $netMask | cut -d '=' -f2)

banner=${banner}$(echo "    Adapter:                 $val\n")
banner=${banner}$(echo "    IP:                      $netIP\n")
banner=${banner}$(echo "    Netmask:                 $netMask\n")
banner=${banner}$(echo "    CIDR:                    $netWork/$netCIDR\n")

fi

done

echo -e $banner

sudo mkdir /opt/redis

echo -e "\033[01m\e[4mType the Adapter, followed by [ENTER]:\e[0m\033[0m"
read nodeAdapter

echo -e "\033[01m\e[4mType the IP of Node1, followed by [ENTER]:\e[0m\033[0m"
read node1

echo -e "\033[01m\e[4mType the IP of Node2, followed by [ENTER]:\e[0m\033[0m"
read node2

echo -e "\033[01m\e[4mType the IP of Node3, followed by [ENTER]:\e[0m\033[0m"
read node3

echo -e "nodeAdapter=$nodeAdapter\nnode1=$node1\nnode2=$node2\nnode3=$node3\n" | sudo tee /opt/redis/node.conf
