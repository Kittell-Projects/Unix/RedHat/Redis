#!/bin/sh

#  Redis.sh
#  
#
#  Created by David Kittell on 6/2/17.
#

clear

# Run Base Template
sh -c "$(curl -s "https://gitlab.com/Kittell-Projects/RedHat/BaseTemplate/raw/master/Base.sh")"

# Run Node Configuration
sh -c "$(curl -s "https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/NodeConfiguration.sh")"

# Run Redis Setup
config="/opt/redis/node.conf"
declare netadapter="$(grep "nodeAdapter" "$config" | awk -F'=' '{print $2}')"
#echo $netadapter
declare ip=$(/sbin/ip -o -4 addr list $netadapter | awk '{print $4}' | cut -d/ -f1)
#echo $ip
declare node1ip="$(grep "node1" "$config" | awk -F'=' '{print $2}')"
#echo $node1ip
declare node2ip="$(grep "node2" "$config" | awk -F'=' '{print $2}')"
#echo $node2ip
declare node3ip="$(grep "node3" "$config" | awk -F'=' '{print $2}')"
#echo $node3ip

# Check if IP of logged in machine is within the node config - start
if grep  -Fq "$ip" "$config"; then
    # IP of logged in machine is within the node config - start
    echo "Accepted Node"
    declare node="$(grep "$ip" "$config" | awk -F'=' '{print $1}')"

    echo "IP: $ip"
    echo "Node: $node"

    # Setup Node non-specific - Start
    # Install needed applications - start
    sudo yum -y install cc gcc-c++ kernel-devel policycoreutils-python
    # Install needed applications - stop
    # Setup Node non-specific - Stop

    # Install Redis (On All Nodes/Servers) - Start
    cd ~/
    wget http://download.redis.io/releases/redis-stable.tar.gz
    tar xzf redis-stable.tar.gz
    rm -f redis-stable.tar.gz
    cd ~/redis-stable
    make
    sudo make install
    sudo rm -rf /etc/redis /var/lib/redis
    sudo rm -f /usr/local/bin/redis-server /usr/local/bin/redis-cli /etc/init.d/redis-server /usr/local/bin/redis-sentinel
    sudo mkdir /etc/redis /var/lib/redis
    sudo cp src/redis-server src/redis-cli src/redis-sentinel /usr/local/bin
    sudo ln -s /usr/local/bin/redis-benchmark /usr/bin/redis-benchmark
    sudo ln -s /usr/local/bin/redis-check-aof /usr/bin/redis-check-aof
    sudo ln -s /usr/local/bin/redis-check-dump /usr/bin/redis-check-dump
    sudo ln -s /usr/local/bin/redis-cli /usr/bin/redis-cli
    sudo ln -s /usr/local/bin/redis-sentinel /usr/bin/redis-sentinel
    sudo ln -s /usr/local/bin/redis-server /usr/bin/redis-server

    # Install Redis (On All Nodes/Servers) - Stop

    # Install Redis Sentinel (On All Nodes/Servers) - Start
    sudo cp sentinel.conf /etc/redis/sentinel.conf.bk
    wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/etc/redis/sentinel.conf
    sudo cp sentinel.conf /etc/redis/sentinel.conf
    sudo mkdir /var/lib/redis/sentinel_26379

    # Open Firewall Port - Start
    sudo firewall-cmd --zone=public --add-port=26379/tcp --permanent
    sudo firewall-cmd --zone=public --add-port=26379/udp --permanent
    # Open Firewall Port - Stop

    # Get Redis-Sentinel-Shutdown - helps shutdown Redis when service call fails - Start
    wget --no-check-certificate -O redis-sentinel-shutdown https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/usr/bin/redis-sentinel-shutdown.sh
    sudo mv redis-sentinel-shutdown /usr/bin/redis-sentinel-shutdown
    sudo chmod 755 /usr/bin/redis-sentinel-shutdown
    # Get Redis-Sentinel-Shutdown - helps shutdown Redis when service call fails - Stop

    # Get Init.d - Start
    wget --no-check-certificate -O redis-sentinel https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/etc/init.d/redis-sentinel
    sudo mv redis-sentinel /etc/init.d/redis-sentinel
    sudo chmod 755 /etc/init.d/redis-sentinel
    sudo systemctl daemon-reload
    # Get Init.d - Stop

    #service redis-sentinel start
    # Install Redis Sentinel (On All Nodes/Servers) - Stop

    # Download and run the node specific installer - start
    case "$node" in
    "node1")
        echo "Welcome Node 1"
        cd ~/
        wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/node1/redis.conf
        wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/node1/sentinel.conf
        wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/opt/redis/redis_maintenance.sh
        wget --no-check-certificate -O redis-shutdown https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/usr/bin/redis-shutdown.sh
        wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/etc/init.d/redis-server

        sed -i "s|10.211.55.9|${node1ip}|" redis.conf
        sed -i "s|10.211.55.9|${node1ip}|" sentinel.conf

        # Get Configuration Files - Start
        cd ~/
        sudo mv redis.conf /etc/redis/redis.conf
        sudo mv sentinel.conf /etc/redis/sentinel.conf
        # Get Configuration Files - Stop

        sudo mv redis_maintenance.sh /opt/redis/redis_maintenance.sh

        # Get Redis-Shutdown - helps shutdown Redis when service call fails - Start
        sudo mv redis-shutdown /usr/bin/redis-shutdown
        sudo chmod 755 /usr/bin/redis-shutdown
        # Get Redis-Shutdown - helps shutdown Redis when service call fails - Stop

        # Open Firewall Port - Start
        sudo firewall-cmd --zone=public --add-port=6379/tcp --permanent # Open Firewall Port
        sudo firewall-cmd --zone=public --add-port=6379/udp --permanent # Open Firewall Port
        # Open Firewall Port - Stop

        # Get Init.d - Start
        sudo mv redis-server /etc/init.d
        sudo chmod 755 /etc/init.d/redis-server
        sudo chkconfig --add redis-server
        sudo chkconfig --level 345 redis-server on
        # Get Init.d - Stop

        # Start Redis-Server and test the configuration - Start
        sudo service redis-server start
        redis-benchmark -h $ip -q -n 1000 -c 10 -P 5 # Test Redis
        redis-cli -h $ip info replication
        #sudo sed -i 's|# requirepass foobared|requirepass RedisSuperSecretPassword|' /etc/redis/redis.conf
        #service redis-server restart

        sudo service redis-sentinel restart
        sudo service redis-server restart
    ;;
    "node2")
        echo "Welcome Node 2"
        cd ~/
        wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/node2/redis.conf
        wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/node2/sentinel.conf
        wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/opt/redis/redis_maintenance.sh
        wget --no-check-certificate -O redis-shutdown https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/usr/bin/redis-shutdown.sh
        wget --no-check-certificate -q https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/etc/init.d/redis-server

        sed -i "s|10.211.55.9|${node1ip}|" redis.conf
        sed -i "s|10.211.55.10|${node2ip}|" redis.conf
        sed -i "s|10.211.55.9|${node1ip}|" sentinel.conf
        sed -i "s|10.211.55.10|${node2ip}|" sentinel.conf

        # Get Configuration Files - Start
        sudo mv redis.conf /etc/redis/redis.conf
        sudo mv sentinel.conf /etc/redis/sentinel.conf
        # Get Configuration Files - Stop

        sudo mv redis_maintenance.sh /opt/redis/redis_maintenance.sh

        # Get Redis-Shutdown - helps shutdown Redis when service call fails - Start
        sudo mv redis-shutdown /usr/bin/redis-shutdown
        chmod 755 /usr/bin/redis-shutdown
        # Get Redis-Shutdown - helps shutdown Redis when service call fails - Stop

        # Open Firewall Port - Start
        sudo firewall-cmd --zone=public --add-port=6379/tcp --permanent # Open Firewall Port
        sudo firewall-cmd --zone=public --add-port=6379/udp --permanent # Open Firewall Port
        # Open Firewall Port - Stop

        # Get Init.d - Start
        sudo mv redis-server /etc/init.d
        sudo chmod 755 /etc/init.d/redis-server
        sudo chkconfig --add redis-server
        sudo chkconfig --level 345 redis-server on
        # Get Init.d - Stop

        # Start Redis-Server and test the configuration - Start
        sudo service redis-server start
        redis-benchmark -h $ip -q -n 1000 -c 10 -P 5 # Test Redis
        redis-cli -h $ip info replication
        #sudo sed -i 's|# requirepass foobared|requirepass RedisSuperSecretPassword|' /etc/redis/redis.conf
        #service redis-server restart

        sudo service redis-sentinel restart
        sudo service redis-server restart
    ;;
    "node3")
        echo "Welcome Node 3"
        cd ~/
        wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/node3/sentinel.conf
        wget --no-check-certificate https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/node3/haproxy.cfg

        sudo sed -i "s|10.211.55.9|${node1ip}|" sentinel.conf
        sudo sed -i "s|10.211.55.10|${node2ip}|" sentinel.conf
        sudo sed -i "s|10.211.55.11|${node3ip}|" sentinel.conf
        sudo sed -i "s|10.211.55.9|${node1ip}|" haproxy.cfg
        sudo sed -i "s|10.211.55.10|${node2ip}|" haproxy.cfg
        sudo sed -i "s|10.211.55.11|${node3ip}|" haproxy.cfg

        # Install HAProxy - Start
        sudo yum -y install haproxy
        # Install HAProxy - Stop

        # Get Configuration Files - Start
        cd ~/
        sudo mv sentinel.conf /etc/redis/sentinel.conf
        sudo mv haproxy.cfg /etc/haproxy/haproxy.cfg
        # Get Configuration Files - Stop

        # Allow haproxy in semanage - sometimes needed - start
        sudo semanage permissive -a haproxy_t
        # Allow haproxy in semanage - sometimes needed - stop

        sudo chkconfig --level 36 haproxy on
        sudo systemctl enable haproxy.service

        # Open Firewall Port - Start
        sudo firewall-cmd --zone=public --add-port=9000/tcp --permanent # Open Firewall Port
        # Open Firewall Port - Stop

        sudo service redis-sentinel restart
        sudo service haproxy restart
    ;;
    esac
    # Download and run the node specific installer - stop

    rm -rf ~/redis-stable
    
    sudo shutdown -r now
    # IP of logged in machine is within the node config - stop

else
    # IP of logged in machine is not within the node config - start
    echo "Unauthorised Node"
    echo "Please verify you have the configuration file correct"
    # IP of logged in machine is not within the node config - stop
fi
