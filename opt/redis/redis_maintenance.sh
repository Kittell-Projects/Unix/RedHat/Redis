#!/bin/bash

clear
echo "Redis Mainenance"
echo "Type 1 to stop all services or 2 to start all services"
read nMaintenance

case $nMaintenance in
    1)
        echo "Shutting Down All Services"
        clear
        sudo redis-shutdown
        sudo service redis-server stop
        sudo redis—sentinel-shutdown
        sudo service redis-sentinel stop
        sudo service haproxy stop
        sudo service redis-server status -l
        sudo service redis-sentinel status -l
        sudo service haproxy status -l
        sudo netstat -tanp | grep redis
        sudo systemctl daemon-reload
    ;;
    2)
        echo "Starting All Services"
        clear
        sudo service redis-sentinel start
        sudo service redis-sentinel status -l
        sudo service redis-server start
        sudo service redis-server status -l
        sudo service haproxy stop
        sudo service haproxy status -l
        sudo netstat -tanp | grep redis
    ;;
    *)
        echo "Invalid Option"
    ;;
esac
