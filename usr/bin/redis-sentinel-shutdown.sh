#!/bin/bash
#
# Wrapper to close properly redis and sentinel
test x"$REDIS_DEBUG" != x && set -x

REDIS_CLI=/usr/bin/redis-cli

# Get the proper config file based on service name
CONFIG_FILE="/etc/redis/redis-sentinel.conf"

# Use awk to retrieve host, port from config file
HOST='127.0.0.1'
PORT=`awk '/^[[:blank:]]*port/ { print $2 }' $CONFIG_FILE`
#AUTH=`awk '/^[[:blank:]]*requirepass/ { print $2 }' $CONFIG_FILE`

echo -e "Host: $HOST\nPort: $PORT"

# shutdown the service properly
$REDIS_CLI -h $HOST -p $PORT shutdown
