# Red Hat Redis
Redis configuration scripts to create one master with two slaves
<h3>Assumptions:</h3>
<ol>
  <li>User has root permissions or is root</li>
  <li>RedHat is already registered with RedHat</li>
</ol>
<h3>How To Install</h3>
<ol>
  <li>sh -c "$(curl -s "https://gitlab.com/Kittell-Projects/RedHat/Redis/raw/master/Redis.sh")"</li>
</ol>
<h3>Server Configuration Example</h3>
In the scripts we will use this configuration, change the IP addresses to match your network.
<ul>
  <li>Node 1 (Master):
    <ul>
      <li>Hostname: devRedis01</li>
      <li>IP: 10.211.55.9</li>
    </ul>
  </li>
  <li>Node 2 (Slave):
    <ul>
      <li>Hostname: devRedis02</li>
      <li>IP: 10.211.55.10</li>
    </ul>
  </li>
  <li>Node 3 (Stand-Alone Sentinel):
    <ul>
      <li>Hostname: devRedis03</li>
      <li>IP: 10.211.55.11</li>
      <li>HAProxy Web Stats URL: http://10.211.55.11:9000/haproxy_stats</li>
      <li>HAProxy Web Stats Port: 9000</li>
    </ul>
  </li>
</ul>